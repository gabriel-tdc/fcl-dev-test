	// Função para obter os parametros inputados na linha de comando
	// Fonte: https://stackoverflow.com/questions/4351521/how-do-i-pass-command-line-arguments-to-a-node-js-program
	function getArgs () {
		const args = {};
		process.argv
			.slice(2, process.argv.length)
			.forEach( arg => {
			// long arg
			if (arg.slice(0,2) === '--') {
				const longArg = arg.split('=');
				const longArgFlag = longArg[0].slice(2,longArg[0].length);
				const longArgValue = longArg.length > 1 ? longArg[1] : true;
				args[longArgFlag] = longArgValue;
			}
			// flags
			else if (arg[0] === '-') {
				const flags = arg.slice(1,arg.length).split('');
				flags.forEach(flag => {
				args[flag] = true;
				});
			}
		});
		return args;
	}

	// Função para transformar o numero float para formato moeda brasileira (x.xxx,xx)
	// Fonte: https://pt.stackoverflow.com/questions/181922/formatar-moeda-brasileira-em-javascript/186798
	function formatMoney(n, c, d, t) {
		c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}

	// Objeto responsavel por obter o valor de acordo com a Origem e Destino
	const tabelaValores = {
		obterValor(origem, destino) {
			var valoresDDD11 = [];
			valoresDDD11[16] = 1.9;
			valoresDDD11[17] = 1.7;
			valoresDDD11[18] = 0.9;
			
			var valorPorMinuto = 0;

			switch (origem){
				case 11:
					if(valoresDDD11[destino]){
						valorPorMinuto = valoresDDD11[destino];
					} else {
						valorPorMinuto = '-';
					}
					break;
				case 16:
					if(destino == 11){
						valorPorMinuto = 2.9;
					} else {
						valorPorMinuto = '-';
					}
					break;
				case 17:
					if(destino == 11){
						valorPorMinuto = 2.7;
					} else {
						valorPorMinuto = '-';
					}
					break;
				case 18:
					if(destino == 11){
						valorPorMinuto = 1.9;
					} else {
						valorPorMinuto = '-';
					}
					break;
				default:
					valorPorMinuto = '-';
					break;
			}

			return valorPorMinuto;
		}
	}

	// Nomes dos planos e o tempo de cada um
	const planos = {
		'FaleMuito 30 ': 30,
		'FaleMuito 60 ': 60,
		'FaleMuito 120': 120,
		'Normal       ': 0
	}

	// Objeto responsavel por calcular o valor de acordo com a origem, destino, plano e minutos
	const faleMuito = {
		calcular(origem, destino, plano, minutos){
			var tempoPlano = planos[plano];
			var valorLigacao = 0;
			var valorPorMinuto = tabelaValores.obterValor(origem, destino);
			var acrescimo = 1;

			if(valorPorMinuto == '-'){
				// Se não está na lista dos valores
				return '-';
			} else {
				// Se calculou corretamente
				if(tempoPlano != 0){
					acrescimo = 1.1;
				}

				if(tempoPlano <= minutos){
					valorLigacao = valorPorMinuto*((minutos-tempoPlano)*acrescimo);
				} else {
					valorLigacao = 0;
				}
				return 'R$ ' + formatMoney(valorLigacao);
			}

		}
	}

	// Obter os parametros inputados na linha de comando
	const parametros = getArgs();

	// Utilizando parseInt para forçar reconhecer como tipo Number
	var inputOrigem		= parseInt(parametros.origem);
	var inputDestino	= parseInt(parametros.destino);
	var inputTempo		= parseInt(parametros.tempo);

	// Exibição na tela
	console.log('Plano         | Valor');
	for (var [key, value] of Object.entries(planos)) {
		console.log(key + ' | ' + faleMuito.calcular(inputOrigem, inputDestino, key, inputTempo));
	}


